# Getting Started with New Angular

_Note: There are two types of Angular. `AngularJS` (version 1.x) and `Angular` (versions 2 and above). Despite the apparent continuity (versions 1 vs 2+), `AngularJS` and `Angular` are really two different frameworks. If you are starting out with Angular, you should really use the new `Angular` not the old `AngularJS`._


As of this writing, the version of Angular is 5.0.x and that of Angular CLI is 1.6.x.

First, install Angular CLI

    npm i -g @angular/cli

Then, create a new Angular app as follows:

    ng new my-new-app --service-worker --routing


The `--service-worker` flag adds PWA support, and you will most likley want that (despite that it's an optional flag in the current CLI, probably due to historical reasons). And, for any non-trivial Angular app, you will likely want routing, and the `--routing` flag adds the necessary code for routing support. (You can always add these later manually, but it's a lot easier to use CLI when you generate a new app.)



_tbd_




## References

* [Angular](https://angular.io/)
* [Angular Docs](https://angular.io/docs)
* [Angular CLI](https://cli.angular.io/)
* [Angular Material](https://material.angular.io/)
* [Angular Universal](https://github.com/angular/universal)
* [angular-ui/ui-router](https://github.com/angular-ui/ui-router)
* [RxJS](http://reactivex.io/rxjs/)
* [ngrx/platform](https://github.com/ngrx/platform)
* []()
* [aspnetcore-angular2-universal](https://github.com/MarkPieszak/aspnetcore-angular2-universal)
* [Running Serverless ASP.NET Core Web APIs with Amazon Lambda](https://aws.amazon.com/blogs/developer/running-serverless-asp-net-core-web-apis-with-amazon-lambda/)
* [Angular Universal and Server Side Rendering Step-By-Step](https://malcoded.com/posts/angular-fundamentals-universal-server-side-rendering)
* [Angular 4 with server side rendering (aka Angular Universal)](https://medium.com/burak-tasci/angular-4-with-server-side-rendering-aka-angular-universal-f6c228ded8b0)
* [Angular Universal Integration](https://github.com/angular/angular-cli/wiki/stories-universal-rendering)
* []()
* []()

